import sys
from PyQt5.QtWidgets import QApplication, QDialog, QInputDialog, QListWidgetItem
from demoListWidgetOp import *


class MyForm(QDialog):
    """docstring for MyForm"""
    def __init__(self):
        super().__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.listWidget.addItem('Ice Cream')
        self.ui.listWidget.addItem('Soda')
        self.ui.listWidget.addItem('Coffee')
        self.ui.listWidget.addItem('Chocolate')
        self.ui.pushButtonAdd.clicked.connect(self.addList)
        self.ui.pushButtonEdit.clicked.connect(self.editList)
        self.ui.pushButtonDelete.clicked.connect(self.delItem)
        self.ui.pushButtonDeleteAll.clicked.connect(self.delAllItems)
        self.show()

    def addList(self):
        self.ui.listWidget.addItem(self.ui.lineEditItem.text())
        self.ui.lineEditItem.setText('')
        self.ui.lineEditItem.setFocus()

    def editList(self):
        row = self.ui.listWidget.currentRow()
        newText, ok = QInputDialog.getText(self, "Enter new text"
                                           , "Enter new text")
        if ok and (len(newText) != 0):
            self.ui.listWidget.takeItem(self.ui.listWidget.currentRow())
            self.ui.listWidget.insertItem(row, QListWidgetItem(newText))

    def delItem(self):
        self.ui.listWidget.takeItem(self.ui.listWidget.currentRow())

    def delAllItems(self):
        self.ui.listWidget.clear()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
