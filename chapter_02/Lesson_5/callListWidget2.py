import sys

from PyQt5.QtWidgets import QDialog, QApplication
from demoListWidget1 import *


class MyForm(QDialog):
    """docstring for MyForm"""
    def __init__(self):
        super().__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.listWidgetDiagnosis.itemClicked.connect(self.dispSelectedtest)
        self.show()

    def dispSelectedtest(self):
        self.ui.label1Test.setText("You Have Selected: " +
                                   self.ui.listWidgetDiagnosis.currentItem()
                                   .text())


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
