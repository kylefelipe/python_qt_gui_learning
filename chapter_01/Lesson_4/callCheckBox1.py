import sys
from PyQt5.QtWidgets import QDialog
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton
from demoCheckedBox1 import *


class MyForm(QDialog):
    """docstring for MyForm."""

    def __init__(self):
        super().__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.checkBoxCheese.stateChanged.connect(self.disAmunt)
        self.ui.checkBoxOlives.stateChanged.connect(self.disAmunt)
        self.ui.checkBoxSausages.stateChanged.connect(self.disAmunt)
        self.show()

    def disAmunt(self):
        amount = 10
        if self.ui.checkBoxCheese.isChecked():
            amount += 1
        if self.ui.checkBoxOlives.isChecked():
            amount += 1
        if self.ui.checkBoxSausages.isChecked():
            amount += 2
        self.ui.labelAmount.setText("Total amount for pizza is " + str(amount))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
