# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'demoLineEdit.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(400, 300)
        self.ButtonClickMe = QtWidgets.QPushButton(Dialog)
        self.ButtonClickMe.setGeometry(QtCore.QRect(140, 260, 83, 28))
        self.ButtonClickMe.setObjectName("ButtonClickMe")
        self.LabelResponse = QtWidgets.QLabel(Dialog)
        self.LabelResponse.setGeometry(QtCore.QRect(10, 80, 371, 151))
        self.LabelResponse.setObjectName("LabelResponse")
        self.widget = QtWidgets.QWidget(Dialog)
        self.widget.setGeometry(QtCore.QRect(10, 30, 381, 30))
        self.widget.setObjectName("widget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.widget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label_2 = QtWidgets.QLabel(self.widget)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout.addWidget(self.label_2)
        self.LineEditName = QtWidgets.QLineEdit(self.widget)
        self.LineEditName.setObjectName("LineEditName")
        self.horizontalLayout.addWidget(self.LineEditName)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.ButtonClickMe.setText(_translate("Dialog", "Click"))
        self.LabelResponse.setText(_translate("Dialog", "TextLabel"))
        self.label_2.setText(_translate("Dialog", "Enter your name"))

