# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'demoCheckBox2.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(471, 273)
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(160, 10, 62, 20))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.label_2.setGeometry(QtCore.QRect(20, 70, 161, 20))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(Dialog)
        self.label_3.setGeometry(QtCore.QRect(290, 70, 191, 20))
        self.label_3.setObjectName("label_3")
        self.labelAmount = QtWidgets.QLabel(Dialog)
        self.labelAmount.setGeometry(QtCore.QRect(70, 230, 321, 20))
        self.labelAmount.setText("")
        self.labelAmount.setObjectName("labelAmount")
        self.groupBoxIceCreams = QtWidgets.QGroupBox(Dialog)
        self.groupBoxIceCreams.setGeometry(QtCore.QRect(10, 90, 231, 121))
        self.groupBoxIceCreams.setObjectName("groupBoxIceCreams")
        self.checkBoxChoclateChips = QtWidgets.QCheckBox(self.groupBoxIceCreams)
        self.checkBoxChoclateChips.setGeometry(QtCore.QRect(10, 30, 191, 26))
        self.checkBoxChoclateChips.setObjectName("checkBoxChoclateChips")
        self.checkBoxChoclateAlmond = QtWidgets.QCheckBox(self.groupBoxIceCreams)
        self.checkBoxChoclateAlmond.setGeometry(QtCore.QRect(10, 70, 171, 26))
        self.checkBoxChoclateAlmond.setObjectName("checkBoxChoclateAlmond")
        self.checkBoxCookieDough = QtWidgets.QCheckBox(self.groupBoxIceCreams)
        self.checkBoxCookieDough.setGeometry(QtCore.QRect(10, 50, 181, 26))
        self.checkBoxCookieDough.setObjectName("checkBoxCookieDough")
        self.checkBoxRockyRoad = QtWidgets.QCheckBox(self.groupBoxIceCreams)
        self.checkBoxRockyRoad.setGeometry(QtCore.QRect(10, 90, 211, 26))
        self.checkBoxRockyRoad.setObjectName("checkBoxRockyRoad")
        self.groupBoxDrinks = QtWidgets.QGroupBox(Dialog)
        self.groupBoxDrinks.setGeometry(QtCore.QRect(250, 90, 211, 121))
        self.groupBoxDrinks.setObjectName("groupBoxDrinks")
        self.checkBoxTea = QtWidgets.QCheckBox(self.groupBoxDrinks)
        self.checkBoxTea.setGeometry(QtCore.QRect(10, 90, 151, 26))
        self.checkBoxTea.setObjectName("checkBoxTea")
        self.checkBoxSoda = QtWidgets.QCheckBox(self.groupBoxDrinks)
        self.checkBoxSoda.setGeometry(QtCore.QRect(10, 60, 151, 26))
        self.checkBoxSoda.setObjectName("checkBoxSoda")
        self.checkBoxCoffee = QtWidgets.QCheckBox(self.groupBoxDrinks)
        self.checkBoxCoffee.setGeometry(QtCore.QRect(10, 30, 151, 26))
        self.checkBoxCoffee.setObjectName("checkBoxCoffee")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.label.setText(_translate("Dialog", "Menu"))
        self.label_2.setText(_translate("Dialog", "Select your IceCream"))
        self.label_3.setText(_translate("Dialog", "Select your drink"))
        self.groupBoxIceCreams.setTitle(_translate("Dialog", "IceCreams"))
        self.checkBoxChoclateChips.setText(_translate("Dialog", "Mint Chocolate Chips $4"))
        self.checkBoxChoclateAlmond.setText(_translate("Dialog", "Chocolate Almond $3"))
        self.checkBoxCookieDough.setText(_translate("Dialog", "Cookie Dough $2"))
        self.checkBoxRockyRoad.setText(_translate("Dialog", "Rocky Road $5"))
        self.groupBoxDrinks.setTitle(_translate("Dialog", "Drinks"))
        self.checkBoxTea.setText(_translate("Dialog", "Tea $1"))
        self.checkBoxSoda.setText(_translate("Dialog", "Soda $2"))
        self.checkBoxCoffee.setText(_translate("Dialog", "Coffe $2"))

