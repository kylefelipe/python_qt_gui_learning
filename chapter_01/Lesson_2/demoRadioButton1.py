# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'demoRadioButton1.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(256, 170)
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(0, 10, 171, 20))
        self.label.setObjectName("label")
        self.labelFare = QtWidgets.QLabel(Dialog)
        self.labelFare.setGeometry(QtCore.QRect(20, 140, 221, 20))
        self.labelFare.setText("")
        self.labelFare.setObjectName("labelFare")
        self.widget = QtWidgets.QWidget(Dialog)
        self.widget.setGeometry(QtCore.QRect(80, 40, 165, 92))
        self.widget.setObjectName("widget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.widget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.radioButtonFirstClass = QtWidgets.QRadioButton(self.widget)
        self.radioButtonFirstClass.setObjectName("radioButtonFirstClass")
        self.verticalLayout.addWidget(self.radioButtonFirstClass)
        self.radioButtonBusinessClass = QtWidgets.QRadioButton(self.widget)
        self.radioButtonBusinessClass.setObjectName("radioButtonBusinessClass")
        self.verticalLayout.addWidget(self.radioButtonBusinessClass)
        self.radioButtonEconomyClass = QtWidgets.QRadioButton(self.widget)
        self.radioButtonEconomyClass.setObjectName("radioButtonEconomyClass")
        self.verticalLayout.addWidget(self.radioButtonEconomyClass)
        self.label.raise_()
        self.labelFare.raise_()
        self.radioButtonBusinessClass.raise_()
        self.radioButtonEconomyClass.raise_()
        self.radioButtonFirstClass.raise_()
        self.radioButtonEconomyClass.raise_()

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.label.setText(_translate("Dialog", "Choose the flight type:"))
        self.radioButtonFirstClass.setText(_translate("Dialog", "First Class $150"))
        self.radioButtonBusinessClass.setText(_translate("Dialog", "Business Class $120"))
        self.radioButtonEconomyClass.setText(_translate("Dialog", "Economy Class $100"))

