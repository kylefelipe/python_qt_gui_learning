# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'demoColorDialog.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(359, 242)
        self.pushButtonColor = QtWidgets.QPushButton(Dialog)
        self.pushButtonColor.setGeometry(QtCore.QRect(10, 30, 111, 28))
        self.pushButtonColor.setObjectName("pushButtonColor")
        self.frameColor = QtWidgets.QFrame(Dialog)
        self.frameColor.setGeometry(QtCore.QRect(140, 10, 201, 141))
        self.frameColor.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frameColor.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frameColor.setObjectName("frameColor")
        self.labelColor = QtWidgets.QLabel(Dialog)
        self.labelColor.setGeometry(QtCore.QRect(10, 180, 331, 20))
        self.labelColor.setText("")
        self.labelColor.setObjectName("labelColor")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.pushButtonColor.setText(_translate("Dialog", "Choose Color"))

