# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'demoInputDialog.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(483, 80)
        self.widget = QtWidgets.QWidget(Dialog)
        self.widget.setGeometry(QtCore.QRect(10, 30, 451, 30))
        self.widget.setObjectName("widget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.widget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(self.widget)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.lineEditCountry = QtWidgets.QLineEdit(self.widget)
        self.lineEditCountry.setObjectName("lineEditCountry")
        self.horizontalLayout.addWidget(self.lineEditCountry)
        self.pushButtonCountry = QtWidgets.QPushButton(self.widget)
        self.pushButtonCountry.setObjectName("pushButtonCountry")
        self.horizontalLayout.addWidget(self.pushButtonCountry)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.label.setText(_translate("Dialog", "Your Country"))
        self.pushButtonCountry.setText(_translate("Dialog", "Choose Contry"))

