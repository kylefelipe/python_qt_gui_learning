import sys
from PyQt5.QtWidgets import QDialog, QApplication
from demoLCD import *


class MyForm(QDialog):
    """docstring for MyForm"""
    def __init__(self):
        super().__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.showLcd)
        timer.start(1000)
        self.showLcd()

    def showLcd(self):
        time = QtCore.QTime.currentTime()
        text = time.toString('hh:mm:ss')
        self.ui.ldcNumber.display(text)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
