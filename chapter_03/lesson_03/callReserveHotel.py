import sys

from PyQt5.QtWidgets import QDialog, QApplication

from reservehotel import *


class MyForm(QDialog):
    """docstring for MyForm"""
    def __init__(self):
        super().__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.roomTypes = ['Suite', 'Super Luxury', 'Super Deluxe', 'Ordinary']
        self.addContent()
        self.ui.pushButton.clicked.connect(self.computeRoomRent)
        self.show()

    def addContent(self):
        for i in self.roomTypes:
            self.ui.comboBox.addItem(i)

    def computeRoomRent(self):
        dateSelected = self.ui.calendarWidget.selectedDate()
        dateInString = str(dateSelected.toPyDate())
        noOfDays = self.ui.spinBox.value()
        chosenRoomType = self.ui.comboBox.itemText(self.ui.comboBox.currentIndex())
        self.ui.EnteredInfo.setText('Date of Reservarion: ' + dateInString +
                                    '\nNumber of Days: ' + str(noOfDays) +
                                    '\nRoom Type Selected: ' + chosenRoomType)
        roomRent = 0
        if chosenRoomType == 'Suite':
            roomRent = 40
        if chosenRoomType == 'Super Luxury':
            roomRent = 30
        if chosenRoomType == 'Super Deluxe':
            roomRent = 20
        if chosenRoomType == 'Ordinary':
            roomRent = 10
        total = roomRent * noOfDays
        self.ui.RoomRentInfo.setText('Room Rent for single day for ' +
                                     chosenRoomType +
                                     '\nType is ' + str(roomRent) +
                                     ' $.\nTotal Room Rent is ' + str(total))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
